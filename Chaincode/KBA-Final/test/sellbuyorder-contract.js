/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { SellbuyorderContract } = require('..');
const winston = require('winston');

const crypto = require('crypto');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logging = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('SellbuyorderContract', () => {

    let contract;
    let ctx;
    const mspid = 'buyer-blockland-com';
    const collectionName = `CollectionOrder`;

    beforeEach(() => {
        contract = new SellbuyorderContract();
        ctx = new TestContext();
        ctx.clientIdentity.getMSPID.returns(mspid);
        ctx.stub.getPrivateData.withArgs(collectionName, '001').resolves(Buffer.from('{"propertyDetails":"5cent,Square Property","buyerName":"AnilKumar","buyerOffer":"60,00,000","assetType":"order"}'));
        const hashToVerify = crypto.createHash('sha256').update('{"propertyDetails":"5cent,Square Property","buyerName":"AnilKumar","buyerOffer":"60,00,000","assetType":"order"}').digest('hex');
        ctx.stub.getPrivateDataHash.withArgs(collectionName, '001').resolves(Buffer.from(hashToVerify, 'hex'));
    });

    describe('#sellbuyorderExists', () => {

        it('should return true for a private asset that exists', async () => {
            await contract.sellbuyorderExists(ctx, '001').should.eventually.be.true;
        });

        it('should return false for a private asset that does not exist', async () => {
            await contract.sellbuyorderExists(ctx, '002').should.eventually.be.false;
        });

    });

    describe('#createSellbuyorder', () => {

        it('should throw an error for a private asset that already exists', async () => {
            await contract.createSellbuyorder(ctx, '001').should.be.rejectedWith('The asset sellbuyorder 001 already exists');
        });

        it('should throw an error if transient data is not provided when creating private asset', async () => {
            let transientMap = new Map();
            ctx.stub.getTransient.returns(transientMap);
            await contract.createSellbuyorder(ctx, '002').should.be.rejectedWith('The privateValue key was not specified in transient data. Please try again.');
        });

        it('should throw an error if transient data key is not privateValue', async () => {
            let transientMap = new Map();
            transientMap.set('prVal', Buffer.from('125'));
            ctx.stub.getTransient.returns(transientMap);
            await contract.createSellbuyorder(ctx, '002').should.be.rejectedWith('The privateValue key was not specified in transient data. Please try again.');
        });

        it('should create a private asset if transient data key is privateValue', async () => {
            let transientMap = new Map();
            transientMap.set('propertyDetails', Buffer.from('5cent,Square Property'));
            transientMap.set('buyerName', Buffer.from('AnilKumar'));
            transientMap.set('buyerOffer', Buffer.from('60,00,000'));
            
            ctx.stub.getTransient.returns(transientMap);
            await contract.createSellbuyorder(ctx, '002');
            ctx.stub.putPrivateData.should.have.been.calledOnceWithExactly(collectionName, '002', Buffer.from('{"propertyDetails":"5cent,Square Property","buyerName":"AnilKumar","buyerOffer":"60,00,000","assetType":"order"}'));
        });

    });

    describe('#readSellbuyorder', () => {

        it('should throw an error for my private asset that does not exist', async () => {
            await contract.readSellbuyorder(ctx, '003').should.be.rejectedWith('The asset sellbuyorder 003 does not exist');
        });

        it('should return my private asset', async () => {
            await contract.readSellbuyorder(ctx, '001').should.eventually.deep.equal({propertyDetails:'5cent,Square Property',buyerName:'AnilKumar',buyerOffer:'60,00,000',assetType:'order'});
            ctx.stub.getPrivateData.should.have.been.calledWithExactly(collectionName, '001');
        });

    });

    

    describe('#deleteSellbuyorder', () => {

        it('should throw an error for my private asset that does not exist', async () => {
            await contract.deleteSellbuyorder(ctx, '003').should.be.rejectedWith('The asset sellbuyorder 003 does not exist');
        });

        it('should delete my private asset', async () => {
            await contract.deleteSellbuyorder(ctx, '001');
            ctx.stub.deletePrivateData.should.have.been.calledOnceWithExactly(collectionName, '001');
        });

    });

    
});
